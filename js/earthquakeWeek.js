/*
 * Earthquake Week JS
 * Thanks for taking a look you source-peeker!
 */

var map;

$().ready(initialize);

function initialize()
{
    map = new L.Map('map', {
        center: new L.LatLng(34,-5.099),
        zoom: 3,
    });


var cloudmadeUrl = 'http://{s}.tile.cloudmade.com/30d1976aef28416ba4e9ed7cdd909ad8/5870/256/{z}/{x}/{y}.png',
    cloudmade = new L.TileLayer(cloudmadeUrl, {maxZoom: 18});

    map.addLayer(cloudmade);

    $.ajax({
        type: 'GET',
        url: 'loader.php?url=http://earthquake.usgs.gov/earthquakes/catalogs/7day-M2.5.xml',
        dataType: 'xml',
        success: parseXml,
        error:  function(req, message) {
            console.log('Error loading map: ' + message);
        }
    });
}

function parseXml(xml)
{
    // retrieve the longitude and latitude string
    var latLong;
    $(xml).find('entry').each(function() {
        var quake = new Object();

        if($.browser.mozilla) {
            latLong = $(this).find('georss\\:point').text();
        } else {
            latLong = $(this).find('point').text();
        }       

        latLongArray = latLong.split(' ');

        var magnitude = $(this).find('title').text().substring(2,5);
        var quake = {
            title: $(this).find('title').text().substring(7),
            latitude: latLongArray[0],
            longitude: latLongArray[1],
            size: magnitude,
            fillColor: getColor(magnitude),
            date: $(this).find('updated').text()
        }
        addQuake(quake);
    });
}

function addQuake(quake)
{
    var colors = getColor(quake.size),
        circleLocation = new L.LatLng(quake.latitude, quake.longitude, true),
        circleOptions = {
            weight: 4, 
            color : colors.stroke,
            fillColor: colors.fill,
            opacity: 0.5,
            fillOpacity: 0.8,
            radius: quake.size*2
        };

    var circle = new L.CircleMarker(circleLocation, circleOptions);//,
    popupContent = '<h3>' + quake.title + '</h3><br/>' +
        '<strong>Magnitude:&nbsp;</strong>' + quake.size + '<br/>' +
        '<strong>Location:&nbsp;</strong>' + quake.latitude + ', ' + quake.longitude + '<br/>';

    circle.bindPopup(popupContent);
    map.addLayer(circle);
}

function getColor(size)
{
    var stroke = '',
        fill = '';

    switch (true) {
        case (size > 7):
            fill = '#FF1141';
            stroke = '#C80D33';            
            break;

        case (size > 6):
            fill = '#FF365E';
            stroke = '#D52D4F';
            break;

        case (size > 5):
            fill = '#FF5D7D';
            stroke = '#D64E69';
            break;

        case (size > 4):
            fill = '#FF829B';
            stroke = '#CE697D';
            break;
        
        case (size > 3):
            fill = '#FFB9C7';
            stroke = '#C9929D';
            break;

        default:
            fill = '#FFD8E0';
            stroke = '#D1B1B8';
    }

    return {
        fill: fill,
        stroke: stroke
    };
}

<?php
/**
 * Javascript can load data from the same domain.
 * However, the xml feed we want isn't.
 * It also isn't available in JSONP format
 * This is essentially a 'proxy' which acts as an intermediary between the remote data and the javascript
 */
$url = $_GET['url'];
echo file_get_contents($url);
